/*
  DMX Mastershield Modified code for Conceptinetics DMX library
  INVIVA project - David Sentkar, Micael David Pereira Santos
  
  Main focus of the program below is to have quick testing possibilites
  
  Copyright (c) 2016 HEAD Haute école d'art et de design Geneve.  All right reserved.
  Code by Gordan Savicic <gordo@yugo.at>
*/


#include <Conceptinetics.h>

/******************************************************************************
 * DMX
 ******************************************************************************/
 
// How many channels are we using? The master will control 100 Channels (1-100)
// (minimum is 1)
#define DMX_MASTER_CHANNELS   16 

// Pin number to change read or write mode on the shield
#define RXEN_PIN                2


// Configure a DMX master controller
DMX_Master        dmx_master ( DMX_MASTER_CHANNELS, RXEN_PIN );

// How many DMX channels we want to address
int LedChannelRange[] = { 2, 3, 4, 5, 7, 8, 10, 11, 13, 14 };
// Length of the array LedChannelRange above
int numberofLedStrings = 10;


/******************************************************************************
 * ULTRASON
 * The MB7360 HRXL-MaxSonar-WR Ultrasonic Sensor is connected like:
 * (Ultrasonic Sensor -> Arduino pin)
 * GND (pin 7) → GND
 * VCC (pin 6) → 5V
 * Envelope (pin 3) → A0
 ******************************************************************************/

 // Define hardware connections
#define PIN_ANALOG_IN A0

// Detection range Threshold (in centimetres change from 0 to 400
int rangeThreshold = 400;

/******************************************************************************
 * Setup routine
******************************************************************************/

void setup() {             
  
  // Enable the DMX shield
  dmx_master.enable ();  
  
  // Set channel range, one DMX Dimmer uses three channels
  // Hence we need to define 15 channels (5 DMX dimmers in series)
  dmx_master.setChannelRange ( 1, 16, 255 );
  
  // Switching all channels off
  channelsOff();
}

/******************************************************************************
 * Endless loop
******************************************************************************/

void loop() 
{
  int value;

  // Check the envelope input
  //value = analogRead(PIN_ANALOG_IN);

  //if (value < rangeThreshold) {
    fadeChannels(LedChannelRange);
  //}
  
}

/* 
 * fadeChannel - will fade up/down specific channel
 * channelNumber {int} - channel to be dimmed
 */
void fadeChannel(int channelNumber) {
  static int dimmer_val;
  for (dimmer_val=0;dimmer_val<256;dimmer_val++) {
    dmx_master.setChannelValue ( channelNumber, dimmer_val );  
    delay ( 5 );
  }
   for (dimmer_val=0255;dimmer_val>0;dimmer_val--) {
    dmx_master.setChannelValue ( channelNumber, dimmer_val );  
    delay ( 5 );
  }
}

void fadeChannels(int channelRange[]) {
  static int dimmer_val;
  for (dimmer_val=0;dimmer_val<256;dimmer_val++) {
    for (int i = 0; i < numberofLedStrings; i++) {
      dmx_master.setChannelValue ( channelRange[i], dimmer_val );  
    }
    delay ( 5 );
  }
  for (dimmer_val=255; dimmer_val>0; dimmer_val--) {
    for (int i = 0; i < numberofLedStrings; i++) {
      dmx_master.setChannelValue ( channelRange[i], dimmer_val );  
    }
    delay ( 5 );
  }
  
  
}

/* 
 * channelOff - will switch off specific channel
 * channelNumber {int} - channel to be switched off
 */
void channelOff(int channelNumber) {
  dmx_master.setChannelValue ( channelNumber, 0 );  
}

/* 
 * channelsOn - will switch ALL channels on
 */
void channelsOn() {
  for(int i=1;i<513;i++) {
    dmx_master.setChannelValue ( i, 255 );  
  }
}
/* 
 * channelsOff - will switch ALL channels off
 */
void channelsOff() {
  for(int i=1;i<513;i++) {
    dmx_master.setChannelValue ( i, 0 );  
  }
}
