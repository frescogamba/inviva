/**
 * Bezier. 
 * 
 * The first two parameters for the bezier() function specify the 
 * first point in the curve and the last two parameters specify 
 * the last point. The middle parameters set the control points
 * that define the shape of the curve. 
 */
int x =0;
float speed = 0.25;
int amplitude = 125;
float temps = 0.;
float WaveSpeed = 0.01;
float phaseShift = 1.5;

void setup() {
  size(640, 360); 
  stroke(255);
  noFill();
  background(0);
  frameRate(25);
}

void draw() {
  //background(0);
  float something = millis()/1000.0;
  temps += (millis()/1000.0)*WaveSpeed;
  float value = amplitude + amplitude * sin( temps * speed * (PI-0.5) - phaseShift );
  print(temps);
  print("  ---   ");
  println(something);
  point(x,height - value);
  x+=1;
  //delay(100);
}

void keyPressed() {
  if (key == CODED) {
    if (keyCode == UP) {
      WaveSpeed += 0.001;
    } else if (keyCode == DOWN) {
      WaveSpeed -= 0.001;
    } 
    else if(keyCode == LEFT) {
      x =0;
      background(0);
    }
  } 
  
  if (speed < 0.0001) {
    speed = 0.00011;
  }
}