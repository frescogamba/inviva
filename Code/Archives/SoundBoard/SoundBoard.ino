/******************************************************************************
 * Connections:
 * The Sound Detector board is connected like:
 * (Sound Detector -> Arduino pin)
 * GND → GND
 * VCC → 5V
 * Envelope → A0
 ******************************************************************************/

 // Define hardware connections
#define PIN_ANALOG_IN A0

void setup()
{
  Serial.begin(9600);

  // Display status
  Serial.println("Initialized");
}

void loop()
{
  int value;

  // Check the envelope input
  value = analogRead(PIN_ANALOG_IN);

  // Convert envelope value into a message
  Serial.println(value);
  
  // pause for 1 second
  delay(500);
}
