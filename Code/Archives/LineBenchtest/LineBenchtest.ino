// Bench test for Inviva Lucis line driver

// Output a slow rect signal on IO7
// Display on LED what is read on IO6
// Read value on A0 and output it to Serial port

#define OUT_PIN 7
#define IN_PIN 6
#define LED 13

int period = 140; // ms
bool val = false;
unsigned long nextWrite = 0;

void setup() {
  // put your setup code here, to run once:
  pinMode(LED, OUTPUT);
  pinMode(OUT_PIN, OUTPUT);
  pinMode(IN_PIN, INPUT_PULLUP);

  Serial.begin(9600);
}

void loop() {
  unsigned long ms = millis();

  //digitalWrite(LED, (millis() / (period / 2)) % 2);


  if (ms >= nextWrite) {
    //digitalWrite(OUT_PIN, (millis() * 2 / period) % 2);
  
    val = !val;
    digitalWrite(OUT_PIN, val);
    
    nextWrite = ms + period / 2;
  }
  
  //delay(period/2);

  digitalWrite(LED, digitalRead(IN_PIN));

  Serial.println(analogRead(A0));
}
