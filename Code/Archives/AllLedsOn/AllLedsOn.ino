#include <DmxSimple.h>

/**************************************************************************
   SETUP routine
 **************************************************************************/
void setup() {
  DmxSimple.maxChannel(4); // Talking to a four channel receiver, so send all four channels.
  Serial.begin(9600);
  // Display status
  Serial.println("Initialized");

}


/******************************************************************************
   MAIN LOOP
 ******************************************************************************/
void loop() {
  DmxSimple.write(1, 255);
  DmxSimple.write(2, 255);
}
