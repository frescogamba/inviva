
/******************************************************************************
 * Connections:
 * The Sound Detector board is connected like:
 * (Sound Detector -> Arduino pin)
 * GND → GND
 * VCC → 5V
 * Envelope → A0
 * The DMX Dimmer is connected like:
 * (DMX Dimmer -> Arduino)
 * DMX in -> 
 * DMX out ->
 * DMX GND -> GND
 ******************************************************************************/

 // Define hardware connections
#define PIN_ANALOG_IN A0

#include <DmxSimple.h>

/**************************************************************************
 * parameters - likely you'll have to change something here
 **************************************************************************/ 
const int lowerLightThreshold = 100;
const int upperLightThreshold = 255;

// SENSIBILITE DE MICRO - CHANGER ENTRE 20 et 150 par exemple
const int minSoundThreshold = 25;
const int maxSoundThreshold = 150;

// sensor readings
const int numReadings =30;

// light behaviour
int fadeOutTime = 100;

/**************************************************************************
 * global variables - very unlikely that you have to change those
 **************************************************************************/ 

int newBrightness = 0;
int brightness;  
boolean fadingOut = false;

int readings[numReadings];      // the readings from the analog input
int readIndex = 0;              // the index of the current reading
long int total = 0;                  // the running total
int average = 0;                // the average
int value = 0;

/**************************************************************************
 * SETUP routine
 **************************************************************************/ 
void setup() {
  DmxSimple.maxChannel(4); // Talking to a four channel receiver, so send all four channels.
  Serial.begin(9600);
  // Display status
  Serial.println("Initialized");
   for (int thisReading = 0; thisReading < numReadings; thisReading++) {
    readings[thisReading] = 0;
  }
}

/******************************************************************************
 * smooth out sensor values from mic (which are read through Analog 0)
 * @params numReadings to get faster/slower updates on sensor values
 * @return {int} smoothed out sensor value
 ******************************************************************************/
int smoothSensor() {
  // subtract the last reading:
  total = total - readings[readIndex];
  // read from the sensor:
  readings[readIndex] = analogRead(PIN_ANALOG_IN);
  // add the reading to the total:
  total = total + readings[readIndex];
  // advance to the next position in the array:
  readIndex = readIndex + 1;
 
  // if we're at the end of the array...
  if (readIndex >= numReadings) {
    // ...wrap around to the beginning:
    readIndex = 0;
  }
 
  // calculate the average:
  average = total / numReadings;
  
  return average;
}

/******************************************************************************
 * MAIN LOOP
 ******************************************************************************/
void loop() {
  
  value = smoothSensor();
  
  // send it to the computer as ASCII digits
  Serial.println(value);
  
  if (value > minSoundThreshold) {
    newBrightness = map(value, 0, 512, lowerLightThreshold, upperLightThreshold);
    Serial.println("*************");
    Serial.println(newBrightness);
    DmxSimple.write(1, newBrightness);
    DmxSimple.write(2, newBrightness);
    fadingOut = true;
  }
  else {
    if (fadingOut == true) {
      fadingOut = false;
      Serial.println("------------");
      for (int i = newBrightness; i > lowerLightThreshold; i--) {
        DmxSimple.write(1, i);
        DmxSimple.write(2, i);
        delay(fadeOutTime);
        
        Serial.println(i);
      }
    }
    newBrightness = 0;
    DmxSimple.write(1, 0);
    DmxSimple.write(2, 0);
  } 
}
