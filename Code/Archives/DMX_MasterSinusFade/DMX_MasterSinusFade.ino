/*
  DMX Mastershield Modified code for Conceptinetics DMX library
  INVIVA project - David Sentkar, Micael David Pereira Santos
  
  Main focus of the program below is to have quick testing possibilites
  
  Copyright (c) 2016 HEAD Haute école d'art et de design Geneve.  All right reserved.
  Code by Gordan Savicic <gordo@yugo.at>
*/


#include <Conceptinetics.h>

// How many channels are we using? The master will control 100 Channels (1-100)
// (minimum is 1)
#define DMX_MASTER_CHANNELS   16 

// Pin number to change read or write mode on the shield
#define RXEN_PIN                2


// Configure a DMX master controller
DMX_Master        dmx_master ( DMX_MASTER_CHANNELS, RXEN_PIN );

float speed = 0.25;
int amplitude = 255;
float temps = 0.;
float WaveSpeed = 0.01;
float phaseShift = 1.5;

/* 
 * Setup routine
 */
void setup() {             
  
  // Enable the DMX shield
  dmx_master.enable ();  
  
  // Set channel range, one DMX Dimmer uses three channels
  // Hence we need to define 15 channels (5 DMX dimmers in series)
  dmx_master.setChannelRange ( 1, 16, 255 );
  
  // Switching channel 1 off
  //channelOff(1);
}

/* 
 * loop - Endless loop
 */
void loop() 
{
  float something = millis()/1000.0;
  temps += (millis()/1000.0)*WaveSpeed;
  float value = amplitude + amplitude * sin( something * speed * (PI-0.5) - phaseShift );
  dmx_master.setChannelValue ( 1, value ); 
  dmx_master.setChannelValue ( 3, value );  
}

/* 
 * fadeChannel - will fade up/down specific channel
 * channelNumber {int} - channel to be dimmed
 */
void fadeChannel(int channelNumber) {
  float something = millis()/1000.0;
  temps += (millis()/1000.0)*WaveSpeed;
  float value = amplitude + amplitude * sin( something * speed * (PI-0.5) - phaseShift );
}

/* 
 * channelOff - will switch off specific channel
 * channelNumber {int} - channel to be switched off
 */
void channelOff(int channelNumber) {
  dmx_master.setChannelValue ( channelNumber, 0 );  
}

/* 
 * channelsOn - will switch ALL channels on
 */
void channelsOn() {
  for(int i=1;i<513;i++) {
    dmx_master.setChannelValue ( i, 255 );  
  }
}
