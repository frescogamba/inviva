
#include <DmxSimple.h>

const int delayTime = 1;
const int lowerThreshold = 50;
const int upperThreshold = 255;

void setup() {
  DmxSimple.maxChannel(4); // Talking to a four channel receiver, so send all four channels.
  Serial.begin(9600);
  // Display status
  Serial.println("Initialized");
}

void loop() {
  int brightness;
  for (brightness = lowerThreshold; brightness <= upperThreshold; brightness++) {
    DmxSimple.write(1, brightness); // Set DMX channel 1 to new value
    DmxSimple.write(2, brightness); // Set DMX channel 1 to new value
    Serial.println(brightness);
    
    delay(delayTime); // Wait 10ms
  }
  for (brightness = upperThreshold; brightness > lowerThreshold; brightness--) {
    DmxSimple.write(1, brightness); // Set DMX channel 1 to new value
    DmxSimple.write(2, brightness); // Set DMX channel 1 to new value
    delay(delayTime); // Wait 10ms
  }
  delay(400);
}

