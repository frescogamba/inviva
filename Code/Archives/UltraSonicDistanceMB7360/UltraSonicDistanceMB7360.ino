/******************************************************************************
 * Connections:
 * The MB7360 HRXL-MaxSonar-WR Ultrasonic Sensor is connected like:
 * (Ultrasonic Sensor -> Arduino pin)
 * GND (pin 7) → GND
 * VCC (pin 6) → 5V
 * Envelope (pin 3) → A0
 ******************************************************************************/

 // Define hardware connections
#define PIN_ANALOG_IN A0

// Detection range Threshold
int rangeThreshold = 350;
  
void setup()
{
  Serial.begin(9600);

  // Display status
  Serial.println("Initialized");
  analogReference(DEFAULT);

}

void loop()
{
  int value;

  // Check the envelope input
  value = analogRead(PIN_ANALOG_IN);
 Serial.println(value);
//  if (value < rangeThreshold) {
//    Serial.println("detected");
//  }
  // pause for 1 second
  delay(25);
}
