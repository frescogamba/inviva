/*
  RemoteSensor
  INVIVA project - David Sentkar, Micael David Pereira Santos

  Main focus of the program below is to control the last box wich has the remote sensor:
  - detect passage of bus or tram using the distance sensor on A0
  - send passing signal on D7 to line driver

  Copyright (c) 2016 HEAD Haute école d'art et de design Geneve.  All right reserved.
  Code by Gordan Savicic <gordo@yugo.at>, Pierre Rossel <pierre.rossel@hesge.ch>
*/


//#include <ResponsiveAnalogRead.h>

/******************************************************************************
   ULTRASON
   The MB7360 HRXL-MaxSonar-WR Ultrasonic Sensor is connected like:
   (Ultrasonic Sensor -> Arduino pin)
   GND (pin 7) → GND
   VCC (pin 6) → 5V
   Envelope (pin 3) → A0
 ******************************************************************************/

// Define hardware connections
//#define PIN_ANALOG_IN A0
#define SENSOR_PWM_IN 5

// Detection range Threshold (in centimetres change from 0 to 1000)
int rangeThreshold = 500;
long pulse, distanceRaw;

// Detection distance threshold (in centimetres change from 0 to 1000
// Must be greater than the distance measured when the smalled bus is present
// Must be smaller thant the distance between the sensor and the ground 
int distThreshold = 150;

// Dist we keep. All other are considered as noise
int distMin = 600;
int distMax = 700;

// Smoothed distance
float distSmooth = 0;

// 1.0 for no damp to 0.0 for full damp
float dampFactor = 0.05;


/******************************************************************************
  SEND PASSAGE SIGNAL on diffential line
  Active low
 ******************************************************************************/

#define PASSAGE_PIN 7


/******************************************************************************
   Setup routine
******************************************************************************/

void setup() {

  // Enable pullup on remote passage pin to avoid noise when not connected
  pinMode(PASSAGE_PIN, OUTPUT);
  digitalWrite(PASSAGE_PIN, HIGH); // Default value: no tram

  // Enable sensor pin as input
  pinMode(SENSOR_PWM_IN, INPUT);
  Serial.begin(9600);

  Serial.println();
  Serial.println("RemoteSensor starting");
  Serial.println();
  Serial.print("Detection range: ");
  Serial.print(distMin);
  Serial.print(" - ");
  Serial.println(distMax);
  Serial.print("Threshold: ");
  Serial.println(distThreshold);
  Serial.print("Damp factor: ");
  Serial.println(dampFactor);
  Serial.println(); 
  Serial.println("Time\tRaw\tDist\tSmooth\tPresence");
  Serial.println("[ms]\t[cm]\t[cm]\t[cm]\t[0-1]");
  Serial.println();
}

/******************************************************************************
   Endless loop
******************************************************************************/

void loop()
{

  // Check the distance sensor (returns 0 if timeout)
  pulse = pulseIn(SENSOR_PWM_IN, HIGH);
  
  // change reported pulse width (58us / cm) to cm
  distanceRaw =  pulse / 58;

  int distance = distanceRaw < distMin ? 0 : (distanceRaw > distMax ? 0: distanceRaw);

  distSmooth += (distance - distSmooth) * dampFactor;

  // Send to master through differential line
  // bool passing = smoothDistance.getValue() < rangeThreshold;
  //bool passing = distanceRaw > 0 && distanceRaw < rangeThreshold;
  bool passing = distSmooth > distThreshold;

  digitalWrite(PASSAGE_PIN, passing ? LOW : HIGH);

  Serial.print (millis());
  Serial.print ("\t");

  Serial.print(distanceRaw);
  Serial.print("\t");
  Serial.print(distance);
  Serial.print("\t");
  Serial.print(distSmooth);
  Serial.print("\t");

  Serial.print (passing);

  Serial.println();
}

