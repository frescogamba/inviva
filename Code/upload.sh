#!/bin/bash

FILE=$1
#HOST=10.42.0.56
HOST=192.168.1.1

if [ "$#" -lt "1" ]; then
    echo "No hex file provided. Usage: ./upload.sh filename.hex "
	    exit 1
		fi

# copy file via scp
		scp -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null $FILE root@$HOST:

		if [[ "$FILE" == *\/* ]] || [[ "$FILE" == *\\* ]]
							  then
							    FILE=`basename $FILE`
								fi
# echo $FILE
# reset arduino and upload with avrdude
ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null root@$HOST "stty -F /dev/ttyACM0 hupcl && avrdude -c arduino -b115200 -P /dev/ttyACM0 -p m328p -u -U flash:w:$FILE"
