/*
  DMX Mastershield Modified code for DMXSimple library
  INVIVA project - David Sentkar, Micael David Pereira Santos

  Main focus of the program below is to control the box 1 (master):
  - detect passage of bus or tram under the master box using the distance sensor on A0
  - detect passage of bus or tram under the remote box 5 using D6
  - generate light animation using DMX to control box 1 to 5

  Serial line commands are available. See help sent in setup() for details

  Copyright (c) 2016 HEAD Haute école d'art et de design Geneve.  All right reserved.
  Code by Gordan Savicic <gordo@yugo.at>, Pierre Rossel <pierre.rossel@hesge.ch>
*/


#include <DmxSimple.h>

// Additional message to dump
String message;

#include "Waves.h"

/******************************************************************************
    CONFIGURATION (see other settings in Waves.h)
 ******************************************************************************/

// The rest brightness, used when nothing else is in progress
#define REST_BRIGHTNESS 255

// How long the waves generation will last, should roughly be the time needed by a bus
// to go from box 1 to 5 or 5 to 1.
int wavesDuration = 15000;
int wavesDurationDbg = 10000;

// Detection distance threshold (in centimetres change from 0 to 1000
// Must be greater than the distance measured when the smalled bus is present
// Must be smaller thant the distance between the sensor and the ground 
int distThreshold = 500;

// Dist we keep. All other are considered as noise
int distMin = 550;
int distMax = 650;

// Set to false to disable remote signal listening, avoiding false remote trigger when the cable
// is not connected
bool listenToRemote = true;

// Simulation of remote bus detection after timeout in idle state. Set to 0 to disable
long remoteSimulationIdleTimeout = 60 * 1000; // [ms]
long remoteSimulationIdleTimeoutDbg = 5 * 1000; // [ms]


/******************************************************************************
    DMX
 ******************************************************************************/

// How many channels are we using? The master will control 100 Channels (1-100)
// (minimum is 1)
#define DMX_MASTER_CHANNELS   16

// Pin number to change read or write mode on the shield
#define RXEN_PIN                2


// How many DMX channels we want to address
// One DMX Dimmer uses three channels, we use 2 of them to drive 2 half arches.
int LedChannelRange[] = { 1, 2, 4, 5, 7, 8, 10, 11, 13, 14 };
// Length of the array LedChannelRange above
#define numberofLedStrings (sizeof(LedChannelRange) / sizeof(*LedChannelRange))



/******************************************************************************
   ULTRASON
   The MB7360 HRXL-MaxSonar-WR Ultrasonic Sensor is connected like:
   (Ultrasonic Sensor -> Arduino pin)
   GND (pin 7) → GND
   VCC (pin 6) → 5V
   Envelope (pin 3) → A0
 ******************************************************************************/

// Define hardware connections
#define PIN_ANALOG_IN A0

// Conversion factor from A0 value to cm
#define ANALOG_TO_CM 2


/******************************************************************************
  REMOTE PASSAGE on diffential line
  Active low
 ******************************************************************************/

#define REMOTE_PASSAGE_PIN 6

/******************************************************************************
  LIGHT ANIMATION
 ******************************************************************************/

// Number of arches
#define NB_ARCHES (numberofLedStrings / 2)

// The bightness value of each arch
int archValue[NB_ARCHES];

// Wave generator from arch 1 to 5
Waves waves15("Waves15", NB_ARCHES, 0);

// Wave generator from arch 5 to 1
Waves waves51("Waves51", NB_ARCHES, 0);

// Wave generator for idle state
Waves wavesIdle("Idle", NB_ARCHES, 120);

// Time of the last wave end
unsigned long msWaveEnd = 0;

// Debug mode (change timings for easier debug)
bool bDebug = false;

// Smoothed distance
float distSmooth = 0;

// 1.0 for no damp to 0.0 for full damp
float dampFactor = 0.3;


/******************************************************************************
   Setup routine
******************************************************************************/

void setup() {

  // Enable pullup on remote passage pin to avoid noise when not connected
  pinMode(REMOTE_PASSAGE_PIN, INPUT_PULLUP);

  // Enable pullup on A0 to avoid noise when no sensor is connected (high value means a long distance, i.e. no bus present)
  //pinMode(A0, INPUT_PULLUP);

  DmxSimple.maxChannel(DMX_MASTER_CHANNELS); // Talking to a four channel receiver, so send all four channels.

  ledChannelsOn(); delay(500); ledChannelsOff(); delay(500); 
  ledChannelsOn(); delay(500); ledChannelsOff(); delay(500); 
  ledChannelsOn(); delay(500); ledChannelsOff(); delay(500); 


  // Switching all channels off
  channelsOff();

  // Configure waves for idle mode
  wavesIdle.waveDuration = 10000;
  wavesIdle.wavePeriod = 10000;
  wavesIdle.archDelay = wavesIdle.wavePeriod / NB_ARCHES; 

  Serial.begin(9600);

  Serial.println("\n\n\n");
  Serial.print("DMX_Master starting with ");
  Serial.print(NB_ARCHES);
  Serial.println(" arches");
  Serial.println();
  Serial.print("Detection range: ");
  Serial.print(distMin);
  Serial.print(" - ");
  Serial.println(distMax);
  Serial.print("Threshold: ");
  Serial.println(distThreshold);
  Serial.print("Damp factor: ");
  Serial.println(dampFactor);
  Serial.println(); 
  Serial.println("Serial commands:");
  Serial.println(" 1      Simulate a bus under box 1");
  Serial.println(" 5      Simulate a bus under box 5");
  Serial.println(" d0/d1  Dis/enable debug mode");
  Serial.println(" r0/r1  Dis/enable listening remote trigger");
  Serial.println();
  //Serial.println("Time\tA0 (~A0)\tDist 1\tBus\t\tLED 1\tLED 2\tLED 3\tLED 4\tLED 5\t§(binary data)");
  Serial.println("Time\tRaw\tDist\tSmooth\tBus\t\tLED 1\tLED 2\tLED 3\tLED 4\tLED 5\t");
  Serial.println("[ms]\t[cm]\t[cm]\t[cm]\t1 - 5");
  Serial.println();
}

/******************************************************************************
   Endless loop
******************************************************************************/

void loop()
{
  unsigned long ms = millis();
  
  message = "";
  
  // Look for commands on serial line
  checkSerial();

  // Check the distance sensor (Vcc/1024 per 2 cm)
  int distanceRaw = analogRead(PIN_ANALOG_IN) * ANALOG_TO_CM;

  int distance = distanceRaw < distMin ? 0 : (distanceRaw > distMax ? 0: distanceRaw);

  distSmooth += (distance - distSmooth) * dampFactor;
  
  bool localPresence = distSmooth > distThreshold;
  if (localPresence) {
    onPresenceUnderBox(1);
  }

  // Check remote passage
  bool remotePresence = digitalRead(REMOTE_PASSAGE_PIN) == LOW;

  if (listenToRemote && remotePresence) {
    onPresenceUnderBox(5);
  }

  Serial.print (ms);
  Serial.print ("\t");

  Serial.print(distanceRaw);
  Serial.print("\t");
  Serial.print(distance);
  Serial.print("\t");
  Serial.print(distSmooth);
  Serial.print("\t");

  Serial.print(localPresence);
  Serial.print(" - ");
  Serial.print(remotePresence);
  Serial.print("\t");

  // Sequence for passage 1 to 5 can set its value
  waves15.update();

  // Sequence for passage 5 to 1 can set its value
  waves51.update();

  wavesIdle.update();
  
  Serial.print ("\t");

  // one wave generator is running ?
  bool bWavesRunning = waves15.isRunning() | waves51.isRunning();
  if (bWavesRunning) {
    msWaveEnd = ms;
  }
  else {

    // Idle state
    
    // Start or keep the idle generator running
    wavesIdle.start(1000);

    // Simulate a bus on remote after a while
    long timeout = bDebug ? remoteSimulationIdleTimeoutDbg : remoteSimulationIdleTimeout;
    if (timeout > 0 && ms - msWaveEnd > timeout) {
      message += "Idle remote simulation. ";
      onPresenceUnderBox(5);
    }
  }

  // Keep the max value of each generator to determine the final value for each arch
  for (int i = 0; i < NB_ARCHES; i++) {

    if (bWavesRunning) {
      //archValue[i] = max(waves15.getValue(i), waves51.getValue(NB_ARCHES - i - 1));
      archValue[i] = min(waves15.getValue(i), waves51.getValue(NB_ARCHES - i - 1));
    }
    else {

//      // Revert to default base luminosity
//      if (archValue[i] != wavesIdle.getValue(i)) {
//        archValue[i] += archValue[i] < wavesIdle.getValue(i) ? 1 : -1;
//      }

      // TODO: add some slow animation between tram passage
      archValue[i] = wavesIdle.getValue(i);
      
    }

    Serial.print(archValue[i]);
    Serial.print("\t");
  }

  Serial.print(message);

  // Separator for binary data
  //Serial.print('§');

  // Send the DMX values. Each arch has 2 DMX chanels
  for (int i = 0; i < NB_ARCHES; i++) {

    // Send values as binary data to the serial line for debug/simulation
    //Serial.write(archValue[i]);

    // Send to DMX controllers
    DmxSimple.write(LedChannelRange[2 * i    ], archValue[i]);
    DmxSimple.write(LedChannelRange[2 * i + 1], archValue[i]);
  }


  Serial.println();
}


void onPresenceUnderBox(int box) {
  if (box == 1) {
    waves15.start(bDebug ? wavesDurationDbg : wavesDuration);
  }
  else if (box == 5) {
    waves51.start(bDebug ? wavesDurationDbg : wavesDuration);
  }
}


void checkSerial() {
  static String cmd;

  while (Serial.available()) {
    char c = Serial.read();

    switch (c) {
      case '\n':
      case '\r':

        if (cmd == "1") {
          onPresenceUnderBox(1);
        }
        else if (cmd == "5") {
          onPresenceUnderBox(5);
        }
        else if (cmd == "d1") {
          Serial.println("\n\nDEBUG MODE ON\n\n");
          bDebug = true;
        }
        else if (cmd == "d0") {
          Serial.println("\n\nDEBUG MODE OFF\n\n");
          bDebug = false;
        }
        else if (cmd == "r1") {
          Serial.println("\n\nLISTEN TO REMOTE ON\n\n");
          listenToRemote = true;
        }
        else if (cmd == "r0") {
          Serial.println("\n\nLISTEN TO REMOTE OFF\n\n");
          listenToRemote = false;
        }

        cmd = "";
        break;

      default:
        cmd += c;
    }
  }
}

/*
   fadeChannel - will fade up/down specific channel
   channelNumber {int} - channel to be dimmed
*/
void fadeChannel(int channelNumber) {
  static int dimmer_val;
  for (dimmer_val = 0; dimmer_val < 256; dimmer_val++) {
    DmxSimple.write ( channelNumber, dimmer_val );
    delay ( 5 );
  }
  for (dimmer_val = 0255; dimmer_val > 0; dimmer_val--) {
    DmxSimple.write ( channelNumber, dimmer_val );
    delay ( 5 );
  }
}

void fadeChannels(int channelRange[]) {
  static int dimmer_val;
  for (dimmer_val = 0; dimmer_val < 256; dimmer_val++) {
    for (int i = 0; i < numberofLedStrings; i++) {
      DmxSimple.write ( channelRange[i], dimmer_val );
    }
    delay ( 5 );
  }
  for (dimmer_val = 255; dimmer_val > 0; dimmer_val--) {
    for (int i = 0; i < numberofLedStrings; i++) {
      DmxSimple.write ( channelRange[i], dimmer_val );
    }
    delay ( 5 );
  }

}

/*
   channelOff - will switch off specific channel
   channelNumber {int} - channel to be switched off
*/
void channelOff(int channelNumber) {
  DmxSimple.write ( channelNumber, 0 );
}

/*
   channelsOn - will switch ALL channels on
*/
void channelsOn() {
  for (int i = 1; i < 513; i++) {
    DmxSimple.write ( i, 255 );
  }
}
/*
   channelsOff - will switch ALL channels off
*/
void channelsOff() {
  for (int i = 1; i < 513; i++) {
    DmxSimple.write ( i, 0 );
  }
}

/*
   channelsOn - will switch ALL channels on
*/
void ledChannelsOn() {
  for (int i = 0; i < numberofLedStrings; i++) {
    DmxSimple.write ( LedChannelRange[i], 255 );
  }
}
/*
   channelsOff - will switch ALL channels off
*/
void ledChannelsOff() {
  for (int i = 0; i < numberofLedStrings; i++) {
    DmxSimple.write ( LedChannelRange[i], 0 );
  }
}
