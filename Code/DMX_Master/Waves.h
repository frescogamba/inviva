/*
  This class generates waves for the arches in Inviva project.

  Copyright (c) 2016 HEAD Haute école d'art et de design Geneve.  All right reserved.
  Code by Pierre Rossel <pierre.rossel@hesge.ch>
*/

class Waves {

  public:

    // *************************************
    // * BEGIN CONFIGURATION
    // *

    // Duration of one wave (the duration of the pulse, from the min to the max to the min).
    // 100 ms is fast, 5000 ms is slow
    int waveDuration = 1000; // ms

    // The time between the start of two waves (should probably be more than the wave duration)
    int wavePeriod = 2000; // ms

    // The propagation delay for a wave to go between one arch to the next
    // a low value means waves going fast fom arch to arch
    int archDelay = 200; // ms

    // *
    // * END OF CONFIGURATION
    // *************************************

  protected:

    const char * name;

    int nbArches = 0;
    int * pValues = 0;

    // The value for the low part of the wave
    int minValue = 0;

    unsigned long startTime = 0;
    unsigned long stopTime = 0;

  public:

    Waves(const char * name, int nbArches, int minValue) {
      this->name = name;
      this->nbArches = nbArches;
      this->minValue = minValue;
      pValues = new int [nbArches];
    }

    ~Waves() {
      delete[] pValues;
      pValues = 0;
    }

    int getValue(int i) {
      return pValues[i];
    }

    void update() {
      unsigned long ms = millis();

      for (int iArch = 0; iArch < nbArches; ++iArch) {

        // Default value
        //pValues[iArch] = minValue;
        pValues[iArch] = 255;

        // Brutal stop for now. TODO: allow waves to finish to last arch before stopping
        if (startTime != 0 && startTime <= ms && ms < stopTime) {
          //          Serial.print("A");
          // time elapsed since start
          long t = ms - startTime;

          // remove the delay between each arch
          t -= archDelay * iArch;

          // convert to a time relative to one period
          t %= wavePeriod;

          // if current time is in a wave
          if (t > 0 && t < waveDuration) {
            // p is a value between 0 and 1, going from 0 to 1 to 0 during waveDuration
            //float p = 0.5 - cos(TWO_PI * t / waveDuration) / 2;
            float p = 0.5 + cos(TWO_PI * t / waveDuration) / 2;
            pValues[iArch] = minValue + (255 - minValue) * p;
          }
        }
        else if (startTime != 0)
        {
          // This cycle is finished
          startTime = 0;
          message += "Stopping ";
          message += name;
          message += " at ";
          message += ms;
          message += " ms. ";
        }
      }
    }

    // Start generating waves for a given duration. If waves are already generating, the stop time is postponed
    // duration  How long we will generate waves. [ms]
    void start(int duration) {
      unsigned long ms = millis();

      if (startTime == 0) {
        startTime = ms;
        message += "Starting ";
        message += name;
        message += " at ";
        message += ms;
        message += " ms. ";
      }
      stopTime = ms + duration;
    }

    // returns true when the generator has been started and has not reached the end of its sequence.
    bool isRunning () {
      return startTime != 0;
    }
};

